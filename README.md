#nlp-solr
##Solr 数据导入引擎
###这里目前提供了一种通过Solrj的远程的方式导入本地 CSV 文件数据的方式，可以通过配置导入CSV文件中指定列到 Solr 中

##使用指南
1. 整个程序利用Spring标准配置文件管理，配置文件地址为：

```
src/main/java/resources/application.xml
```
####里面对每一个参数都有详细的说明，同时对应的类中也有详细的注释。

2. 运行AppStart类	（注意，如果你需要在运行之前删除一些索引可以在SolrUtils里面编写相关代码。）

##业务拓展
1. 想要从数据库中直接导入索引到solr中，那更加简单，
####参考网址：
http://blog.csdn.net/christophe2008/article/details/6299225

2. 如果想要导入csv和数据库导入统一起来，参考技术：csvjdbc
####参考网址：
http://skyuck.iteye.com/blog/1225297 

