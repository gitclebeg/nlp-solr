package eshore.cn.it.solrhandler;

import java.nio.charset.Charset;
import java.util.ArrayList;

import com.csvreader.CsvReader;


public class CSVReaderTest {
	/**
     * 读取CSV文件
     */
    public static void  readeCsv() {
    	try {    
             int maxNumber = 1;
             ArrayList<String[]> csvList = new ArrayList<String[]>(); //用来保存数据
             String csvFilePath = "F:/广州亿迅科技有限公司工作记录/沈哥/pro_wo_forma.csv";
             CsvReader reader = new CsvReader(csvFilePath,',',Charset.forName("GBK"));    //一般用这编码读就可以了    
             
             System.out.println("delimiter:" + reader.getDelimiter());
             System.out.println("recordDelimiter:" + reader.getRecordDelimiter());
             System.out.println("textQualifier:" + reader.getTextQualifier());
             reader.setTextQualifier(' ');
             reader.readHeaders(); // 跳过表头   如果需要表头的话，不要写这句。
             for (String str : reader.getHeaders())
        		 System.out.print(str + ",");    
        	 System.out.println("");
             int i = 1;
             while(reader.readRecord() && i <= maxNumber) { //逐行读入除表头的数据    
                 csvList.add(reader.getValues());
                 i++;
             }            
             reader.close();
             for(int row = 0; row < csvList.size(); row++){
            	 for (String str : csvList.get(row))
            		 System.out.print(str + ",");    
            	 System.out.println("");
             } 
             
         }catch(Exception ex) {
             System.out.println(ex);
         }
    }
     
    public static void main(String[] args) {
    	 readeCsv();
	}
}
