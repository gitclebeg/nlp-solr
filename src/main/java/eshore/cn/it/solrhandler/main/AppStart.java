package eshore.cn.it.solrhandler.main;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import eshore.cn.it.solrhandler.SolrUtils;
import eshore.cn.it.solrhandler.hand.FileIndexHandler;

public class AppStart {
	private static String appFileName = "classpath:resources/application.xml";
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		BeanFactory factory = new ClassPathXmlApplicationContext(appFileName); 
		
		SolrUtils processor = (SolrUtils) factory.getBean("solrUtils");
		processor.deleteIndex(null);
		
		FileIndexHandler fih = (FileIndexHandler) factory.getBean("fileIndexHandler");
		fih.doFileIndex();

	}

}
