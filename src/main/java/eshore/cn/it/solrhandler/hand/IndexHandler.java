package eshore.cn.it.solrhandler.hand;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import eshore.cn.it.solrhandler.config.SolrApp;

/**
 * 通过此类可以向Solr中提交索引
 * @author clebeg
 */
public abstract class IndexHandler {
	
	/**
	 * 需要提交的字段的索引下标
	 */
	protected int indexIds[];

	public int[] getIndexIds() {
		return indexIds;
	}

	public void setIndexIds(int[] indexIds) {
		this.indexIds = indexIds;
	}
	
	/**
	 * 需要建立索引的列名
	 */
	protected String[] indexColNames;
	
	public String[] getIndexColNames() {
		return indexColNames;
	}
	@Required
	public void setIndexColNames(String[] indexColNames) {
		this.indexColNames = indexColNames;
	}
	/**
	 * 对应列名，这个列名在solr里面都是唯一的，上面的索引只是这个列集合中的一部分
	 */
	protected String[] colNames;
	public String[] getColNames() {
		return colNames;
	}

	/**
	 * 设置需要建立索引的列名，同时计算索引
	 * @param colNames
	 */
	public void setColNames(String[] colNames) {
		this.colNames = colNames;
	}

	protected abstract void destroyIndexHandler();
	
	/**
	 * 初始化索引建立者，消除先后属性的先后依赖，配置一些必要的参数
	 */
	protected abstract void initIndexHandler();

	
	/**
	 * 此方法就是为上面的数据建立索引的，这里不再考虑数据多少提交，由传输给他的类控制数据一次提交的大小
	 * @throws IOException 
	 * @throws SolrServerException 
	 */
	public void doIndexTask(List<String[]> indexDatas) throws SolrServerException, IOException {
		List<SolrInputDocument> sids = new ArrayList<SolrInputDocument>();
		for (String[] data : indexDatas) {
			SolrInputDocument doc = new SolrInputDocument();
			for (int id : this.getIndexIds())
				doc.addField(this.getColNames()[id], data[id]);
			sids.add(doc);
		}
		this.getSolrApp().getSolrClient().add(sids);
		this.getSolrApp().getSolrClient().commit();
	}
	
	
	@Autowired
	protected SolrApp solrApp;
	
	public SolrApp getSolrApp() {
		return solrApp;
	}
	public void setSolrApp(SolrApp solrApp) {
		this.solrApp = solrApp;
	}
}
