package eshore.cn.it.solrhandler.config;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

/**
 * 远程solr连接的全局类
 * @author clebeg
 */
public class SolrApp {
	//solr连接的入口，连接某个core或者其它的
	private String baseUrl;
	
	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	
	public SolrClient getSolrClient() {
		return new HttpSolrClient(this.getBaseUrl());
	}
	
	
}
